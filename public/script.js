// Replace with your Google API Client ID
const CLIENT_ID = '593088919616-ds5575qhna4p9793ebbo4h5d0qtr9kq8.apps.googleusercontent.com';
const REDIRECT_URI = 'https://robertsodergren.gitlab.io'; // Must match the redirect URI in Google Cloud Console
const SCOPES = 'https://www.googleapis.com/auth/tasks';

// DOM elements
const authButton = document.getElementById('auth-button');

const tasklistsContainer = document.getElementById('tasklists-container');

// Handle OAuth 2.0 flow
function handleAuthClick() {
  const authUrl = `https://accounts.google.com/o/oauth2/v2/auth?client_id=${CLIENT_ID}&redirect_uri=${REDIRECT_URI}&response_type=token&scope=${SCOPES}`;
  window.location.href = authUrl;
}

// Check for access token in the URL after redirect
function checkForAccessToken() {
  const hash = window.location.hash.substring(1);
  const params = new URLSearchParams(hash);
  const accessToken = params.get('access_token');

  if (accessToken) {
    // Hide the auth button
    authButton.style.display = 'none';
	document.getElementById('bottom-of-page-container').style.display = 'block';
    // Fetch task lists using the access token
//exampleUpdateTask(accessToken);
    fetchTaskLists(accessToken);
  }
}

// Patch a task in tasklist from Google Tasks API
async function patchTaskInList(accessToken, taskListId, taskId, updatedTask) {
  try {
    var endpoint = 'https://www.googleapis.com/tasks/v1/lists/' + taskListId + '/tasks/' + taskId;
    
    var headers = {
       Authorization: `Bearer ${accessToken}`,
       'Content-Type': 'application/json'
    };
    
    var options = {
       'method': 'PATCH',
       'headers': headers,
       'payload': JSON.stringify(updatedTask)
    };
    
    const response = await fetch(endpoint, options);

    if (!response.ok) {
      throw new Error(`HTTP error! Status: ${response.status}`);
    }

  } catch (error) {
    document.write('Error patching task in list:', error);
  }
}

// Fetch task lists from Google Tasks API
async function fetchTaskLists(accessToken) {
  try {
    const response = await fetch('https://tasks.googleapis.com/tasks/v1/users/@me/lists', {
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    });

    if (!response.ok) {
      throw new Error(`HTTP error! Status: ${response.status}`);
    }

    const data = await response.json();
    displayTaskLists(data.items, accessToken);
  } catch (error) {
    console.error('Error fetching task lists!:', error);
  }
}

// Display task lists in the DOM
function displayTaskLists(taskLists, accessToken) {
  tasklistsContainer.innerHTML = ''; // Clear existing content

  if (taskLists && taskLists.length > 0) {
	taskLists.sort((a, b) => {
	  const nameA = a.title.toUpperCase(); // ignore upper and lowercase
	  const nameB = b.title.toUpperCase(); // ignore upper and lowercase
	  if (nameA < nameB) {
		return -1;
	  }
	  if (nameA > nameB) {
		return 1;
	  }

	  // names must be equal
	  return 0;
	});
    taskLists.forEach(taskList => {
      const taskListDiv = document.createElement('div');
      taskListDiv.classList.add('tasklist-item');
      taskListDiv.innerHTML = `<h2>${taskList.title}</h2>`;
      tasklistsContainer.appendChild(taskListDiv);

      // Fetch tasks for this task list
      fetchTasks(taskList.id, accessToken, taskListDiv);
    });
  } else {
    tasklistsContainer.innerHTML = '<p>No task lists found.</p>';
  }
}


function exampleUpdateTask(accessToken) {
  var taskListId = 'MTA3NDAyMzczODQ2NjYzMTY1OTg6MDow';
  var taskId = 'ekZXWGhzOEZBQ3ZvQU1TTw';
 
  var updatedTask = {
    status: 'Updated Task Title',
    notes: 'Updated task notes',
    due: '2025-02-25T00:00:00.000Z'
  };

  var result = patchTaskInList(accessToken, taskListId, taskId, updatedTask);
}

// Fetch tasks for a specific task list
async function fetchTasks(taskListId, accessToken, container) {
  try {
    const response = await fetch(`https://tasks.googleapis.com/tasks/v1/lists/${taskListId}/tasks`, {
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    });

    if (!response.ok) {
      throw new Error(`HTTP error! Status: ${response.status}`);
    }

    const data = await response.json();
    displayTasks(data.items, container);
  } catch (error) {
    console.error('Error fetching tasks:', error);
  }
}

// Display tasks in the DOM
function displayTasks(tasks, container) {
  const taskListUl = document.createElement('ul');
  taskListUl.classList.add('task-list');

  if (tasks && tasks.length > 0) {
    tasks.sort((a, b) => Date.parse(a.due) - Date.parse(b.due));
    tasks.forEach(task => {
      const taskItem = document.createElement('li');
      taskItem.classList.add('task-item');
      taskItem.innerHTML = `
        <input type="checkbox" class="myinput large custom" />
	<strong> ${task.title}</strong>
        <span><i>${task.notes != undefined && task.notes != ' ' ?  ' → ' + task.notes : ''}</i>${task.due != undefined ? ' → ' + task.due.substring(0,10) : ''}</span>
      `;
      taskListUl.appendChild(taskItem);
    });
  } else {
    taskListUl.innerHTML = '<li>No tasks found.</li>';
  }

  container.appendChild(taskListUl);
}


window.addEventListener(
  "touchmove",
  function (event) {
    if (event.scale !== 1) {
      event.preventDefault();
      event.stopImmediatePropagation();
    }
  },
  { passive: false }
);
// Event listener for the auth button
authButton.addEventListener('click', handleAuthClick);

// Check for access token when the page loads
window.onload = checkForAccessToken;